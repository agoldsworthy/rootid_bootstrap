(function($) 
{
	$(document).ready(function() {
		//paragraphs case studies
		$('.field-name-field-case-reference > div ').owlCarousel({
		    loop:true,
		    margin:10,
		    nav:true,
		    responsive:{
		        300:{
		            items:1
		        },
		        600:{
		            items:2
		        },
		        1000:{
		            items:3
		        }
		    }
		});

		//related content
		$('.view-related-content .view-content').owlCarousel({
		    loop:true,
		    margin:10,
		    nav:true,
		    responsive:{
		        300:{
		            items:1
		        },
		        600:{
		            items:3
		        },
		        1000:{
		            items:4
		        }
		    }
		});

	});
})(jQuery);
