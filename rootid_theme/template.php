<?php

/**
 * @file
 * template.php
 */

function limit_text($text, $limit) {
      if (str_word_count($text, 0) > $limit) {
          $words = str_word_count($text, 2);
          $pos = array_keys($words);
          $text = substr($text, 0, $pos[$limit]) . '...';
      }
      return $text;
    }

function rootid_theme_menu_tree__main_menu($variables) {
  return '<ul class="menu nav nav-pills nav-left">' . $variables['tree'] . '</ul>';
}